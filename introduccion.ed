; Hola, esta es la sintaxis de ED en acción, empezando por este comentario :)
; Los comentarios son solo para este formato y pueden escribirse con esta sintaxis;
; los comentarios no son conservados en los formatos de salida y son eliminados antes de la conversión

--- ; inicia el área de metadatos en sintaxis YAML (los comentarios también pueden ir al final de la línea)
; Cfr. Sitio oficial de YAML: https://yaml.org

; El primer metadato de este documento es 'title'
title: Introducción a las :meditaciones de :marco

; El segundo metadato es el nombre de la obra de :marco
meditaciones: _Meditaciones_
; para los metadatos que son texto se puede usar sintaxis ED/MD (como las _itálicas_) 

; El tercer metadato es el autor de :meditaciones
marco: Marco Aurelio

; ¿Por que está escrito ':marco' y ':meditaciones' en lugar
; «Marco Aurelio» y «_Meditaciones_»?
; Cualquier llave que no sea otra llave, como 'marco' y 'meditaciones',
; puede usarse en cualquier lugar de este documento
; esta llave es reemplazada por su contenido durante la conversión.

; Esto hace que las llaves de estos metadatos se usen como _constantes_ en este documento.

; Para usar las llaves como constantes, estas tienen que marcarse en este texto.
; La sintaxis para las constantes es ':' más el nombre de la llave,
; Ejemplo 1: :marco para usar como constante de la llave 'marco' cuyo valor es «Marco Aurelio»
; Ejemplo 2: :meditaciones para la llave 'meditaciones' con valor «_Meditaciones_»
; Ejemplo 3: :title para 'title', es decir, «Introducción a las _Meditaciones_ de Marco Aurelio»

; No hay un esquema de datos necesario, aunque con el tiempo se descubrirán recomendaciones
tags: intro

; Para usar constantes de llaves dentro de llaves solo continua la sintaxis;
; Ejemplo: :llave1:llave2b para 'false' o :llave1:llave2a:llave3  para '[item1, item2, item3]'
llave1:
  llave2a:
    llave3:
    - item1
    - item2
    - item3
  llave2b: false
  llave2c: 1.0
  llave2d: 0
  llave2e: HOLA

; Si la llave es un conjunto, puede especificarse el separador para unirlo como una cadena de texto
; De no ponerse por defecto es ' / '
; Por ejemplo, para llave1:llave2a quedaría como item1 / item2 / item3
; Entonces, de quererse modificar este comportamiento, puede indicarse con la llave 'joiner'
joiner: ', '
; Esto hace que 'llave1:llave2a' se vea como «item1, item2, item3»

; Para las llaves con booleano pueden cambiarse su valores por defectos ('true' y 'false') con 'bool'
bool:
  t: verdadero
  f: falso
---

; Con  los ámbitos de aplicación puedes tener fragmentos (snippets) exclusivos para ciertos unos.
; Por ejemplo, para hacer pruebas:
@scope[prueba](
; Si usas '--scope prueba' lo siguiente aparecerá en el archivo convertido:
Soy una prueba de ámbitos de aplicación.
)@

; Puedes declarar diversos ámbitos de aplicación para un snippet,
; esto permite reutilizarlos para usos distintos.
; Por ejemplo, el siguiente snippet se incrustaría en los ámbitos de prueba
; o para un documento que tendrá una hipotética salida a 'epub'
@scope[prueba, epub](
; Puedes indicar más de un ámbito de aplicación como '--scope prueba,epub'
; para aplicar varios ámbitos en una conversión.
Soy otra prueba de ámbitos de aplicación
)@

@scope[prueba](
Prueba de constantes:

- llave1 (Hash a String):   :llave1 (debe quedarse ':llave1')
- llave3 (Array a String):  :llave1:llave2a:llave3
- llave2b (Bool a String):  :llave1:llave2b
- llave2c (Float a String): :llave1:llave2c
- llave2d (Int a String):   :llave1:llave2d
- llave2e (String):         :llave1:llave2e
)@

; Con la constante ':title' se hace innecesario repetir el texto del título
# :title 

; Es posible una forma abreviada de encabezado en el que
; con número de indica su jerarquía de 1 a 6.
#2 Una historia y apreciación moderna de las :meditaciones[^1]

Las :meditaciones (c. 170) de :marco yacen entre las sorpresas
de la literatura. Con pocas probabilidades este texto fue escrito para ser un libro,
con menos probabilidades hubiese llamado la atención para el porvenir. No hay
evidencia que sugiera que su existencia era conocida por sus contemporáneos,
aunque en los círculos filosóficos de Roma pudo haber sido leído y citado con
avidez. Los historiadores, quienes celebraron las virtudes de :marco,
no conocieron este escrito. Nueve siglos después, cuando el griego había
caído en el olvido en Europa, unos cuantos extractos hicieron su camino,
fueron aceptados y traducidos.

Hasta el Renacimiento, en 1558, el texto íntegro fue traducido al latín y publicado
en Alemania por Guilielmus Xylander[^2] a partir de un manuscrito hoy perdido.
Desde ese día las :meditaciones tuvieron un lugar en la literatura a través de
la redición y la publicación en diversos idiomas.

![
Figura 1. Portadilla de la edición príncipe de las :meditaciones publicada por
Xylander en Zúrich, 1559.
(Fuente: Wikimedia Commons)
](local:figura1.png remoto:https://i.imgur.com/yNUermS.png)

En este lapso de tiempo la autoría de las :meditaciones no ha sido puesta en
duda con rigurosidad, ya que ningún manuscrito o certificado de autenticidad es necesario.
El propio texto es evidencia suficiente y convincente porque no es la antología
de un sofista, sino un supuesto diario privado para el alivio que conlleva la autoexaminación.
Las referencias o reminiscencias son breves y personales, lo que permite establecer
la autenticidad de la obra.

Para obtener el valor de las :meditaciones, este texto ha de leerse como
un conjunto de pensamientos personales y poco sofisticados para uno mismo, lo cual
impide tener algún vestigio de autoridad. Párrafo tras párrafo, la tinta utilizada
es por lo general inconsecuente, informal u ocasional; algunas veces es un recordatorio
o una cita; otras veces, un argumento, reflexión o aforismo que poco a poco se
vuelven familiares e inteligibles gracias al hombre que lo escribió y no por
tratarse del discurso de un emperador, filósofo o autor.

Las meditaciones sobre la templanza, la prudencia, la fortaleza y la justicia no son
una mera enumeración de virtudes cardinales. En su lugar, están amoldadas de
manera fresca dentro de los límites de las formulaciones estoicas. Este
escrito es una autobiografía mejor comprendida y más íntimamente conocida que
los pensamientos de Platón o Aristóteles. En sus meditaciones :marco se vuelve
familiar para aquellos que se dedican a la autocomunión.

La única preocupación de :marco son sus problemas personales. Su esfuerzo
reside en confrontar los hechos, ya que estos afectan la moral y allanan las ilusiones
para llegar al corazón de los motivos. Esto conduce a cada acto, abstención,
afecto, impulso, memoria y aspiración al criterio de la verdad universal. Bajo esa
luz se ponen a prueba como valores vitales, que se justifican o se repudian, en pos
de la armonía del individuo con el universo.

Una comparación con Epicteto,[^3] maestro espiritual de :marco, ayudará a
clarificar la especificidad de su perspectiva moral. Epicteto es el maestro cuyo
trabajo es el de aplicar la filosofía a todas las distintas circunstancias y ocupaciones;
su ojo siempre está en su audiencia, su tono se adapta a cualquier tipo de hombre;
esclavos o patriarcas, comerciantes o soldados, estudiantes o atletas,
él tiene interés en todos por igual. Por este motivo su trabajo
discurre sobre modales no menos que la moral; adereza sus pláticas con
ilustraciones concretas; apela a la literatura y a la historia tanto como
a anécdotas de filósofos o personajes.

Con las :meditaciones de :marco se respira una atmósfera distinta.
No hay una misión ni se percibe una búsqueda por provocar un efecto o un anhelo de
novedad. No hay objeciones por refutar o una lección que dar; no cuenta con una
audiencia mixta por atraer y mantener, ni una diversidad de circunstancias por
considerar. Las virtudes o los vicios de otros son ignorados. Desde la primera
hasta la última página solo tiene un severo auditor: él mismo. Su actitud es
insistente y no se presta a ligerezas mientras se examina a sí mismo con la voz
analítica de la razón, la cual disecciona impresiones y difumina las ilusiones
del movimiento y el color.

## Una historia y apreciación contemporánea[^4]

Los primera edición de las :meditaciones en español fue publicada por Jacinto
Díaz de Miranda[^5] en 1785 como _Los doce libros del emperador :marco_,
aunque esta edición en la actualidad se le conoce más como los _Soliloquios_ de
:marco. Con el tiempo el título «:meditaciones» se emplearía de manera
más habitual para este texto, por lo que en esta edición se ha preferido conservar
esta nomenclatura.

![
Figura 2. Portadilla de la edición de las :meditaciones publicada por Jacinto
Díaz de Miranda en Madrid, 1785.
(Fuente: Internet Archive)
](local:figura2.png remoto:https://i.imgur.com/YREhl3b.png)

Curiosamente en el mundo hispanohablante el interés por la vida y obra de :marco
se remontan a unas décadas antes de la edición príncipe de Xylander. En
1528 Antonio de Guevara[^6] publicó la novela epistolar _Libro áureo del
emperador :marco_ o _Relox de príncipes_. Esta obra fue un sorprendente
éxito editorial, ya que se publicaron 58 ediciones en español, latín, francés,
italiano, inglés, alemán, danés y holandés entre los siglos XVI y XVII. Para
situar este insospechado éxito, Marcelino Menéndez Pelayo[^7] sostiene que gozó de tanta
popularidad como _Amadís de Gaula_ o _La Celestina_. Sin embargo, esta fama vino
acompañada de severas críticas debido a sus imprecisiones y ficciones históricas,
entre las que se incluye la crítica de Jacinto Díaz de Miranda. A pesar de ello,
esta novela establecería una de las bases para el reencuentro renacentista con
el texto de :marco.

Aunque los comentadores modernos consideran a las :meditaciones como un diario,
en la actualidad esta aseveración se considera una imprecisión, debido a que
no hay referencias de fechas ni de lugares que permitan entender el texto como
un escrito íntimo. No obstante, se mantiene la apreciación como una obra sin
palabras accesorias y con una inclinación moral. Otra imprecisión de los comentaristas
modernos de las :meditaciones es que omitieron la mención de la única copia
manuscrita que queda de este texto. Este se encuentra en el códice _Vaticanus Graecus
1950_ (el número no hace referencia a una fecha), elaborado en c. 1300. Hoy en día
se encuentra en la Biblioteca del Vaticano, así como está disponible gratuitamente
en línea.[^8]

En comparación a otros estoicos como Epicteto o Séneca,[^9] :marco no
destaca como un filósofo original ni complejo. Su estilo reside en la síntesis
filosófica en forma de ética. :marco es un escritor menos hábil que Séneca pero
con un elegante estilo confesionario. En relación con el estilo de Epicteto, :marco
no es tan ágil y es menos optimista. En sus :meditaciones no hay aspectos teóricos de
física o de gnoseología de la escuela estoica. Aunque es perceptible un ideal de
sabio como modelo, no pretende ser un maestro de la virtud. Quizá lo más atractivo
de su escrito es la sinceridad que se acompaña del intento riguroso de :marco
por seguir sus principios éticos. Este rigor ético le daría la reputación de un
emperador filósofo y humanista.

Las :meditaciones tienen su atractivo como una doctrina vivida en vez de predicada
de la ética estoica. Se conforma de ejercicios espirituales que reformulan dogmas y
tópicos estoicos para fortalecer y abreviar un discurso interior que pretende vivir
de acuerdo a la naturaleza. :marco distingue dos aspectos del discurso interior,
uno objetivo y otro subjetivo. El primero se caracteriza por describir la realidad,
mientras que el segundo consiste en intervenciones pasionales ajenas a los hechos.
Ante la necesidad de vivir coherentemente según la naturaleza, :marco concluye
que el aspecto subjetivo del discurso interior debe suprimirse a partir del uso
riguroso de la razón.

La autonomía de la razón y la rigurosidad de la autoexaminación son las principales
características de las :meditaciones. Su estilo aforístico o epigramático, además de
repetitivo, permite que esta obra sea accesible para nosotros sin necesidad de ahondar
demasiado en el contexto de la filosofía estoica o de la vida de :marco,
aunque su conocimiento no evita tener una interpretación más nutrida de las :meditaciones.

Otra característica de estos soliloquios es su punto de partida en las inquietudes
del hombre común por el paso del tiempo y la fragilidad de la vida. Esta aproximación
hace que el uso de autoridades sea innecesario. Un aspecto adicional de las :meditaciones
es que su búsqueda por una unión original con el cosmos no conlleva alguna sacralidad.
Por ello, las :meditaciones son una muestra de la psicología de :marco: un hombre
con un gran interés en el ascetismo y alguien con un profundo descontento interior que
le induce un afán de mejoría. Esta insistencia hacen de las :meditaciones un ejemplo
destacable de autoexaminación y autocrítica que el lector puede aprehender para sí mismo.

## El emperador filósofo[^10]

¿Quién fue, entonces, ese emperador filósofo y hombre insatisfecho que escribió las
:meditaciones? :marco nació el 26 de abril de 121 en Roma. Fue hijo del político
Marco Annio Vero y de la noble Domicia Lucila. Se sabe que tuvo una hermana dos años
más joven que él, Annia Cornificia Faustina. Su padre fue pretor[^11] del Senado romano
hasta su repentina muerte en c. 124, cuando :marco tan solo tenía 3 años. Desde entonces
estuvo bajo la tutela de su madre y de su abuelo, quien falleció cuando tenía 19 años.
En estos primeros años :marco estrecharía los lazos con su madre, la cual por
su piedad, generosidad y sencillez, según atestigua en sus :meditaciones, lo acercarían
tiempo después al estoicismo.

![
Figura 3. Busto del joven :marco, siglo XVIII.
(Fuente: Christie's)
](local:figura3.png remoto:https://i.imgur.com/HrYMVvN.png)

Aunque :marco vino de una familia noble de Roma y fue cercano al emperador Adriano,
su designio como sucesor al trono fue producto de varias coincidencias. El 
emperador Adriano había designado como heredero al noble Lucio Elio César, a la vez que :marco
fue prometido con la hija de este noble romano. Sin embargo, la frágil salud de
Lucio Elio César hizo que muriera antes de Adriano. Por ese motivo, el emperador modificó el
orden de sucesión al trono. Antonino Pío, que en ese momento se desempeñaba como uno de
los cuatro procónsules (el máximo cargo magisterial en Roma), fue elegido por Adriano
como su sucesor con la condición de que el hijo del fallecido Lucio Elio César, Lucio Vero,
y :marco fuesen los siguientes en la línea sucesoria. Además, Adriano también anuló
el compromiso matrimonial de :marco para comprometerlo con la hija de su nuevo
sucesor. Tras la muerte de Adriano, Antonino Pío cumplió su palabra y desde su juventud
:marco fue designado como uno de los herederos al trono. 

Desde sus primeros años :marco llamaría la atención de sus contemporáneos por su
inteligencia y compromiso. Por ello es que Adriano lo percibía como una persona honesta,
confianza que le ayudó para la ascensión social descrita con anterioridad. A través de las cartas
de su tutor, Marco Cornelio Frontón, también se sabe que :marco tenía una inclinación
por la filosofía desde temprana edad, escribía obras en griego y en latín (hoy perdidas) y
disfrutaba de la obra de Epicteto.

Conforme pasaron los años, :marco empezó a desempeñar un papel más activo en la
vida pública de Roma. En 140 fue designado cónsul por Antonino Pío y en 147 se desempeñaría
como procónsul. Por esos años, en 145, se casó con la hija de Antonino, Annia Galeria Faustina.
Estos acontecimientos hicieron que :marco se perfilara sin inconvenientes como
próximo emperador de Roma.

En 161 Antonino Pío murió por una fiebre, así que :marco ascendió al trono bajo la
explícita condición de que Lucio Vero fuese también nombrado Augusto (título reservado a los
emperadores romanos). Esta condición hizo que Lucio Vero procurara lealtad
a :marco. La compartición del trono permitió que :marco delegara las
responsabilidades militares a Lucio Vero, el cual era reconocido por su talento y experiencia
militar. Lucio Vero además era admirado por las legiones romanas y, como efecto secundario,
evitó que :marco nombrara a un general para que se hiciera cargo de estas responsabilidades.
Esta organización limitó las aspiraciones de varios generales, acto relevante si se
toma en cuenta los precedentes de Julio César y Vespasiano, militares que de manera sangrienta
se hicieron del trono del Imperio romano en un pasado no tan remoto para :marco.

![
Figura 4. Busto del emperador :marco, c. 170.
(Fuente: Wikimedia Commons)
](local:figura4.png remoto:https://i.imgur.com/iAQ7LEi.png)

En sus primeras funciones como emperador, :marco emitió reformas para limitar
los abusos de la jurisprudencia civil. Además reconoció las relaciones de sangre en torno a
la sucesión de títulos, una medida que fue favorable para los esclavos, las viudas y los bastardos.
Estas actividades y su personalidad le hicieron ganarse la gracia y el respeto del pueblo
romano. 

La pujante actividad de :marco y Lucio Vero fue ensombrecida por las siguientes situaciones
que afectaron la vida en Roma. Primero, la muerte de Antonino propició una guerra en contra del
Imperio Parto en Armenia y Siria, así como provocó invasiones de bárbaros en Britania
y Germania. Lucio Vero personalmente se encargaría de la campaña en contra del Imperio Parto,
la cual finalizó con éxito en 166. En Britania los romanos fueron obligados a replegarse de
nuevo al muro de Adriano, una posición alcanzada por el Imperio romano en c. 122. Esta
situación permaneció sin cambios hasta después de la muerte de Lucio Vero y :marco.
En Germania, las invasiones llevaron a ambos emperadores a emprender varias acciones militares
que continuarían hasta después de sus muertes.

La otra situación que ensombreció el reinado de :marco y Lucio Vero fue la peste
antonina, una epidemia de viruela o sarampión que duró de 165 a 180. Esta peste fue
traída por el ejército de Lucio Vero después de la guerra en contra del Imperio Parto.
Las estimaciones contemporáneas sobre su mortandad oscilan entre un rango de dos a treinta
por ciento de romanos fallecidos. En ese rango, muchos asienten que al menos un décimo de
la población del Imperio romano pereció por la peste, aproximadamente 7.5 millones de personas,
siendo los soldados los mayores afectados.

A estas dificultades se suma la muerte de Lucio Vero en 169. No existe consenso sobre su
causa, pero las dos hipótesis más aceptadas son la enfermedad de la epidemia o el envenenamiento.
La ausencia de Lucio Vero hizo que :marco se enfocará en la defensa romana frente a los
bárbaros de Germania. Con un ejército languidecido por la peste, :marco permaneció en
esta situación hasta el 17 de marzo de 180, fecha en la que muere, al parecer por la peste,
en lo que hoy es Viena.

Vale la pena hacer un paréntesis respecto a su fama como emperador filósofo y humanista.
Aunque :marco fue querido entre sus súbditos y realizó reformas para mejorar sus
condiciones, su celebridad se opaca un poco debido al trato que dio a los cristianos.
Al parecer :marco veía a los cristianos como una secta de fanáticos ajenos a los
intereses de Roma. Además, en lo particular le parecía ilusoria la creencia cristiana de
una vida mejor como recompensa después de la muerte. Por la sensibilidad reconocida de
:marco, diversas comunidades cristianas creyeron encontrar en él un emperador
benévolo. Sin embargo, durante su reinado :marco no detuvo ni sentenció las represiones
y las masacres de estas comunidades. Su ímpetu de estoicismo racionalista al parecer le
impedía tener empatía por las religiones mistéricas como el cristianismo. Esta falta de
empatía se percibe en su omisión para la impartición de la justicia a favor de los
cristianos.

## La filosofía estoica[^12]

Para terminar con esta introducción, ahora se lleva a cabo un brevísimo comentario sobre
la filosofía estoica. Zenón de Citio[^13] se considera el fundador de esta filosofía.
Zenón y sus discípulos se reunían en la Stoa Pecile, un vistoso pórtico a unos pasos
al norte del ágora ateniense. Por el nombre de este punto de reunión, la escuela
fundada por Zenón se conoció como escuela estoica. Otros representantes relevantes del
estoicismo antiguo y medio son Crisipo de Sole[^14], Panecio de Rodas[^15] y Posidonio
de Apamea.[^16] Los principales representantes del estoicismo posterior son Séneca,
Epicteto y el mismo :marco.

![
Figura 5. Reconstrucción de la perspectiva occidental de la Stoa Pecile, 1981.
(Fuente: ASCSA Digital Collections)
](local:figura5.png remoto:https://i.imgur.com/DgWRDma.png)

La gnoseología, la física y la ética fueron áreas del conocimiento trabajadas extensamente
por los estoicos. Para esta escuela el origen del conocimiento es la sensibilidad. El
intelecto es una tabla rasa que se llena de las representaciones que ofrece la
percepción sensible. Es decir, el conocimiento surge a partir de la copia y la representación,
donde el criterio para distinguir lo verdadero de lo falso reside entre el estado
«normal» de los órganos sensoriales y una distancia crítica que permite asentir si
una sensación es un hecho o una fantasía. 

La física estoica se caracteriza por un materialismo compuesto de una
«fuerza» o «energía» que impide percibir a la muerte como un límite entre el ser y
la nada. En su lugar, la muerte se concibe como una diferencia de «grado» en la participación
de esta fuerza vital. Una consecuencia que se desprende de esta manera de ver a la
naturaleza es que no hay ser ni fuerza ni energía trascendente sino inmanente, por lo que
se tiene una concepción panteísta del universo. Esta visión del mundo además es cíclica, en
donde el fin de cada periodo hace que todo regrese y vuelva a brotar de nuevo. Esta
característica pone de manifiesto que en la filosofía estoica el mundo es una totalidad
dinámica estrictamente ordenada. Cabe destacar que pese al uso de referencias
mitológicas y religiosas, la escuela estoica se apega a una comprensión naturalista
del mundo: cualquier rasgo mitológico o religioso sobre la naturaleza, el mundo o el
universo solo se emplea como metáfora para exponer sus puntos de vista.

Estas concepciones del mundo tienen una influencia directa en la ética estoica.
Esta área de conocimiento hizo célebre tanto a :marco como al estoicismo.
Sin embargo cabe advertir que esta ética es más conocida por sus prescripciones
prácticas que por su profundidad teórica.

Al estar todo permeado por la sensibilidad, el alma es corpórea aunque
al mismo tiempo es imposible de localizar en el cuerpo como si se tratase de un
órgano. El alma, así entendida, está presente en una especie de unidad con el cuerpo
que queda afectada por un impulso natural que coloca al hombre en un vaivén.
A la razón le corresponde controlar y encausar los afectos, lo que al mismo
tiempo implica una división entre los afectos nobles e innobles. Un afecto innoble es
aquel que se da de manera irreflexiva y espontánea. Para el estoico la verdad
no es un acto momentáneo sino un hecho objetivo y constante, por lo que los afectos innobles
se consideran ilusorios. La consecuencia puntual de esta noción sobre los afectos
hace que el dolor o el placer se cataloguen como ilusiones. Los afectos nobles son
aquellos que surgen de manera reflexiva gracias al uso de la razón. Esto conlleva
un entendimiento de las virtudes como el uso recto de la razón: uno de los tópicos a
tratar por :marco en sus :meditaciones.

Otra característica relevante de la razón en relación con la ética estoica es que 
esta se considera una condición suficiente para aprehender un conocimiento de lo
que es justo o injusto. Debido a que la razón se considera repartida de modo equitativo
en todos los hombres, para el estoicismo no existe persona que sea incapaz de
diferenciar lo justo de lo injusto. Una consecuencia interesante de esta concepción
de la razón, que influyó en la noción del «buen sentido» de Descartes, es que todos
los hombres son iguales sin importar su condición, sea esclavo, mujer o menor de edad,
ni su procedencia, se trate de un romano o de un bárbaro.

Este derecho natural sobre la igualdad entre los hombres distaba mucho de las concepciones
antropológicas del derecho romano. Por ejemplo, en Roma los esclavos se consideraban
meros instrumentos, las mujeres no eran sujetos de derecho y los menores de edad
eran excluidos de la vida pública. Sin embargo, el influjo estoico entre los
nobles romanos coadyuvó a la mejora de las condiciones de vida de amplios sectores
de la población del Imperio romano, por lo que esta concepción de derecho natural
cabe catalogarse como humanista. El _ethos_ cosmopolita del estoicismo también tuvo
afinidades espirituales con el cristianismo. No obstante, esta ha de entenderse de
manera general y con el recordatorio de que la bondad, la justicia y la verdad para
el estoicismo no residen ni tienen su fuente en una entidad externa al mundo, como
lo supone el cristianismo al ver en su dios el principio de todas las cosas.

Para la ética estoica la felicidad reside en una vida conforme a la naturaleza, la
ley y la razón (tres elementos en consonancia). Para esta escuela, la eudemonía es
consecuencia de un ejercicio de las virtudes, que llevan al estoico a desdeñar la
exterioridad de la razón como los bienes, el honor, la riqueza, la pobreza, los achaques
corporales e incluso la vida o la muerte. Un efecto colateral de este ideal de felicidad
es que la teoría por sí misma es incapaz de llevar a la eudemonía, por lo que la
práctica de la virtud es tan importante como su teorización o especulación.

La insistencia por la práctica de la virtud caracterizan al estoico como un hombre
de voluntad que se esfuerza en seguir a la razón a pesar de los embates de las afecciones
del alma. Pero esta constante pugna consigo mismo no lleva a interpretar al estoicismo
como una escuela que percibe al camino de la virtud a modo de un sendero a recorrer en
soledad y aislamiento. Al contrario, el mayor uso de la razón se da en la participación
del hombre en la vida pública. Ahí es en donde la razón tiene las máximas posibilidades
de acción, lo que permite resaltar dos particularidades de la vida activa del estoicismo.
Primero, denota la paridad entre la razón, la naturaleza y la ley: si el uso de
la razón es el seguimiento del orden de la naturaleza, si la ley es la expresión 
política concreta del uso de la razón, entonces la ley es o debe constituirse según
este orden natural. Segundo, la ética estoica se muestra afín a las necesidades
políticas imperiales: a mayor poder y extensión del dominio, mayores posibilidades
para una vida activa.

La última característica a resaltar de la ética estoica es su entendimiento sobre el
destino y la libertad. El estoico es fatalista ya que el destino es inmutable por lo
que al hombre solo le queda llegar a buenos términos con esta verdad. Esta postura
es la que más ha causado polémica dentro de la ética estoica debido a una patente
paradoja. Por un lado el estoico defiende la importancia del uso de la razón; es decir,
el hombre es libre de usar rectamente su razón o de estar en un constante vaivén
por sus afecciones. Por el otro, esta libertad individual es irrelevante para una
ley natural inmutable; en otros términos, la libertad brilla por su ausencia en un
universo dinámico y ordenado a través de diferentes ciclos. La manera
general en como se intenta subsanar esta paradoja es con la indicación de que el hombre
por capricho es libre de oponerse al orden natural, así como es libre de reconocer
y aceptar el designio de la naturaleza.

Esta solución, de ser certera, nos coloca en una posición en donde la libertad y el
destino no son antagónicos, sino que el primero se trata de una capacidad del individuo mientras
que el segundo consiste en una verdad objetiva. De ser así, la libertad se caracterizaría
por una limitación fundamental: el hombre es libre de todo, excepto de la necesidad
de tener que lidiar con su propia libertad; a saber, la decisión ética de obrar conforme
a la razón o según sus afecciones. De tratarse de una solución aparente, la ética
estoica pone en manifiesto una resignación cansada ante un horizonte en el que no
hay esperanza ni acción que evite la reproducción del _statu quo_.

En un mundo en crisis y que en distintos lugares se encamina hacia la guerra, y
en una situación global paralizada por años debido a una pandemia, las :meditaciones
de :marco nos interpelan de manera crítica y sin un dejo de arcaísmo:

> la corrupción del espíritu es peste ciertamente más nociva que la destemplanza e infección
del aire a nuestro alrededor esparcido, porque ésta es peste de los vivientes en cuanto
son animales; pero aquélla lo es de los hombres en cuanto son racionales.

; Se pueden incluir cualquier fragmento de texto (snippet) con @include
@include(notas.ed)
; Es importante tomar en cuenta que los archivos que tengan al inicio metadatos en YAML, estos NO serán incluidos.
